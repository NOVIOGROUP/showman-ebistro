import 'package:font_awesome_flutter/font_awesome_flutter.dart';

// TODO: 4-Update SmartChat Features
const kConfigChat = {
  "EnableSmartChat": true,
};

/// config for the chat app
/// config Whatapp: https://faq.whatsapp.com/en/iphone/23559013
const smartChat = [
  {'app': 'https://wa.me/12428045644', 'iconData': FontAwesomeIcons.whatsapp},
  {'app': 'tel:12428045644', 'iconData': FontAwesomeIcons.phone},
  {'app': 'sms://12428045644', 'iconData': FontAwesomeIcons.sms},
  {'app': 'firebase', 'iconData': FontAwesomeIcons.google},
  {
    'app': 'https://tawk.to/chat/5e5cab81a89cda5a1888d472/default',
    'iconData': FontAwesomeIcons.facebookMessenger
  }
];
const String adminEmail = "noviogroup@gmail.com";
