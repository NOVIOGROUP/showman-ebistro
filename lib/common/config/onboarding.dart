// TODO: 4-Update Welcome Screens
/// the welcome screen data
/// set onBoardingData = [] if you would like to hide the onboarding
List onBoardingData = [
  {
    "title": "Welcome to FluxStore",
    "image": "assets/images/fogg-delivery-1.png",
    "desc": "Taste The World ",
    "background": "#000056"
  },
  {
    "title": "Connect Surrounding World",
    "image": "assets/images/fogg-uploading-1.jpg",
    "desc":
        "Follow our recipes and Cook it Up  "
            "Fast, convenient and Fun.",
    "background": "#000036"
  },
  {
    "title": "Let's Get Started",
    "image": "fogg-order-completed.jpg",
    "desc": "Waiting no more, let's see what we get!"
  },
];
